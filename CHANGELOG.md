## 0.14.0

### New features

- Add command to update a Release !147

### Maintenance

- Build and publish darwin/arm64 binaries !142
- Fix goimports violation !143
- Update Go to 1.18.4 !141
- Update sys package to address CVE-2022-29526 !140 (Florent Monbillard @f.monbillard)
- Use a common Release response struct !148
- Upgrade Go to 1.19.1 !151

### Documentation changes

- Fix typo in usage help text !146 (chenzeming @chenzeming)
- Add documentation for update command !150

### Other changes

- Replace gosec-sast with semgrep-sast job !149

## 0.13.0~beta.3.g891caad (2022-07-18)

### Bug fixes

- Revert Update Dockerfile to run as unprivileged user !138

## 0.12.0

### New features

- Create annotated tag by passing --tag-message !132
- Update Dockerfile to run as unprivileged user !127 (Av1o @av1o)

### Maintenance

- Use Dangerbot from common gem !131
- Upgrade go to 1.18 !129

### Documentation changes

- Move architecture description to development docs !128

### Other changes

- Replace feature::maintenance with  type::maintenance !126

## 0.11.0

### New features

- Add --debug flag to show info about request and response !115

### Bug fixes

- fix: null date parse error !118

### Maintenance

- refactor: remove '--assets-links-name' and '--assets-links-url' !114 (feistel @feistel)

### Documentation changes

- Update Release-CLI CONTRIBUTING.md !120

### Other changes

- Stop running secret_detection for trains !122
- fix: linter errors !121 (Furkan Türkal @Dentrax)
- Create AssetMarshaller for asset link parsing !119

## 0.10.0

### New features

- feat: build and publish binary for linux/ppc64le !113 (feistel   @feistel)

### Bug fixes

- fix: parse date for milestones correctly !116
- fix: set empty CI_COMMIT_TAG for test-windows !112

### Other changes

- build: bump securejoin to 0.2.3 !111 (feistel   @feistel)

## 0.9.0

### New features

- Disable CGO for building binaries in CI !106
- Upload binaries to generic package !108
- Add get command to app !100

### Documentation changes

- Link to .gitlab-ci.yml docs !95

### Other changes

- Refactor: Move away from ioutil (deprecated) !109 (feistel   @feistel)
- Fix typo/omission of "CLI" in the Readme !107
- Set empty DefaultText for tokens !105
- Add GetRelease to GitLab Client !99
- Fix tabs and local references in Makefile !98
- Use default container-scanning analyzer !96

## 0.8.0

### New features

- Add custom user agent !92

### Maintenance

- Regenerate test SSL certificates !93

## 0.7.0 (2021-04-09)

### New Features

- Add support for ADDITIONAL_CA_CERT_BUNDLE !86

### Documentation changes

- Add note for PowerShell users !88
- add warning to example about assets needing login !83 (Colin Macdonald @cbm755)

### Other changes

- Update Go version to 1.16.3 !89

## 0.6.0

### New Features

- Support use of PRIVATE-TOKEN to create releases using the CLI !32
- Distribute `release-cli` via S3 !76

### Documentation changes

- Changed `except` to `rules` in Release CLI docs !79

### Other changes

- Add junit reports to CI !81 (Nejc Habjan @nejch1)
- Use single testdata directory for /internal folder !80 (Kevin Kengne @kevin.kengne1)

## 0.5.0

### New Features

- Read release description from file if present !67 (Nejc Habjan @nejch1)

### Bug fixes

- Add timeout flag !66

### Documentation changes

- Add a note about minimum Go version !73
- Update wording about generic package !64

### Other changes

- Bump Go version to 1.15.5 !72
- Update release team and labels !71
- Update to latest go version !70
- Don't run secret detection job for merge trains or tags !69
- Fix typo in documentation !62
- Add example for release assets as Generic package !61
- Use templates again for security scanners !60
- Improve pipeline execution !58

## 0.4.0 (2020-09-08)

### New features

- Add filepath and link_type to GitLab client !48
- Add --assets-link flag to support direct asset links !49

### Maintenance

- Deprecate `--assets-links-name` and `--assets-links-url` !49
- Resolve "Do not override Docker latest tag for backports" !46
- Break up CI configuration into includes by stage !45
- Add container scanning to release-cli !39
- Fix secret detection jobs !54
- Use correct dependency-scanning job !56

### Other changes

- Create a release from a tag !51
- Add @pedropombeiro as reviewer !47
- Update changelog generator to accept new labels !44

## 0.3.0 (2020-07-14)

### New features

- Support sending assets to the Releases API !31
- Add asset flags to create command !33
- Add `milestone` and `released_at` to release-cli params !38
- Add `milestones` and `released_at` to the GitLab Client !37

### Bug fixes

- Resolve "Run pipelines on new tags" !30

### Maintenance

- Update `latest` Docker tag !36
- Make name and description optional !40

### Other changes

- Return early if no asset names specified !42

## v0.2.0 (2020-05-21)

### New features

- Build binaries for different OSs !17
- Add danger reviews !11

### Maintenance

- Run jobs `on_success` instead of always !29
- Use security templates for analysis !27
- Update to use mockery:v.1.1.0 !25
- Fix error parsing for releases API !22 (Sashi @ksashikumar)
- Add integration tests with mock server !12

### Documentation changes

- doc: Remove shell markdown for usage blocks !23 (Elan Ruusamäe @glensc)
- Document verisioning process !21

## v0.1.0

- Create Release CLI module [!4](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/4)
- Add GitLab client package [!5](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/5)
- Add command `create` [!6](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/6)
- Add docker image [!13](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/13)
