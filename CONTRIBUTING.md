## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Code of conduct

As contributors and maintainers of this project, we pledge to respect all people
who contribute through reporting issues, posting feature requests, updating
documentation, submitting pull requests or patches, and other activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal appearance,
body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other contributions that are
not aligned to this Code of Conduct. Project maintainers who do not follow the
Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported by emailing contact@gitlab.com.

This Code of Conduct is adapted from the [Contributor Covenant](https://contributor-covenant.org), version 1.1.0,
available at [https://contributor-covenant.org/version/1/1/0/](https://contributor-covenant.org/version/1/1/0/).

## Changelog

GitLab's Release CLI keeps a [changelog](CHANGELOG.md). 

Changelog entries are generated each time a new Release is created. 

The process to generate the content is outlined in this [Release issue template](https://gitlab.com/gitlab-org/release-cli/-/blob/master/.gitlab/issue_templates/release.md). You can [create a new Release issue](https://gitlab.com/gitlab-org/release-cli/-/issues/new?issuable_template=release) now.

## Release CLI Maintainers

| Maintainer         |
|--------------------|
|@steveazz|
|@jaime|

## Development Process

GitLab's Release CLI follows the engineering process as described in the [handbook][eng-process]. The exceptions are that our [issue tracker][release-cli-issues] is on GitLab's Release CLI
project and there's no distinction between developers and maintainers. Every team member is equally responsible for a successful master pipeline and fixing security issues.

Merge requests need to be **approved by at least one
[GitLab Release team member](https://about.gitlab.com/handbook/engineering/development/ops/release/#team-members)
AND a [maintainer](#release-cli-maintainers)**.

[eng-process]: https://about.gitlab.com/handbook/engineering/workflow/
[release-cli-issues]: https://gitlab.com/gitlab-org/release-cli/issues/

## Style Guide

We follow GitLab's [Go standards and style guidelines](https://docs.gitlab.com/ee/development/go_guide/).

