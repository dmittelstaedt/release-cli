package gitlab

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseDateTime(t *testing.T) {
	tests := []struct {
		name     string
		dateTime string
		errMsg   string
	}{
		{
			name:     "success",
			dateTime: "2019-03-15T08:00:00Z",
		},
		{
			name:     "success_with_milliseconds",
			dateTime: "2019-03-15T08:00:00.999Z",
		},
		{
			name:     "success_with_valid_timezone",
			dateTime: "2019-03-15T08:00:00+10:00",
		},
		{
			name:     "invalid_format_with_invalid_timezone",
			dateTime: "2019-03-15T08:00:00 -0700",
			errMsg:   "cannot parse",
		},
		{
			name:     "invalid_format",
			dateTime: "2019/03/15T08:00:00Z",
			errMsg:   "cannot parse",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := ParseDateTime(tt.dateTime)
			if tt.errMsg != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.errMsg)
			} else {
				require.NoError(t, err)
			}
		})
	}
}
